1 заменил #pragma push (pack) на #pragma pack(push,1) и соответсвенно #pragma pop

2 переименовал time.h|c в board_time.h|c, конфликтуют с системным time.h

3 REPO_CMD.C скобки внутри массива

4     HAL_UART_Receive_DMA(GPS_state.uart, &GPS_state.dma_storage, N_GPS_DATA_DMA - 1); 
   исправил на &GPS_state.dma_storage[0]

5 заменил sprintf на snprintf - сказано sprintf #ifndef _REENT_ONLY

6 заменил бинарные константы на hex

7 log_sd_str("ISL.txt", &sd_log_str, strlen(sd_log_str)); на log_sd_str("ISL.txt", sd_log_str, strlen(sd_log_str));


9 Передачи и прием по ДМА должен принимать указатель на u8, принимает u8** (Sun_sensor, GPS)

10)	В функции 	if(fw_settings.fw_size_current >= FLASH_APP_SIZE){ 
   Всегда false первое число сравнения 16 бит, константа 32 бита

11 	Функции Callback переделать как в заголовочнике (static void uart_rx_cb(UART_HandleTypeDef * h);

12 Функции fwupdate_* должны возвращать int и принимать параметры (u8, u8*) 

13 CS для SD карты на PG5, в файле sd.h указано 
#define SPI_SS_Pin GPIO_PIN_8
#define SPI_SS_GPIO_Port GPIOA 

14 Часы работают от LSI а не LSE. Контроллер также не тактируется от кварца

15 Файл sd.c SD_PowerOn  while (timer - HAL_GetTick()   < 20) – поменять местами
