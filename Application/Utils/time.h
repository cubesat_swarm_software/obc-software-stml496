/*
 * time.h
 *
 *  Created on: 12 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef UTILS_TIME_H_
#define UTILS_TIME_H_
#include "stm32l4xx_hal.h"

#pragma push(pack)
#pragma pack(1)
struct time_board {
	uint8_t seconds;
	uint8_t minutes;
    uint8_t hours;
    uint8_t days;
};
#pragma pop(pack)

void    time_get(struct time_board* time);
uint8_t time_set(RTC_TimeTypeDef *sTime,RTC_DateTypeDef *sDate);

#endif /* UTILS_TIME_H_ */
