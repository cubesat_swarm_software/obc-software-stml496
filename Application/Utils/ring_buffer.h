#ifndef RING_BUFFER_H
#define RING_BUFFER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

typedef struct
{
    uint8_t *buffer;
    uint16_t idxIn;
    uint16_t idxOut;
    uint16_t size;
} RING_buffer_t;

typedef enum
{
    RING_ERROR = 0,
    RING_SUCCESS = !RING_ERROR
} RING_ErrorStatus_t;

uint16_t RING_CRC16ccitt(const RING_buffer_t *buf, uint16_t lenght, uint16_t position);

uint16_t RING_CRC16ccitt_Intermediate(const RING_buffer_t *buf, uint16_t lenght, uint16_t tmpCrc, uint16_t position);

RING_ErrorStatus_t RING_Init(RING_buffer_t *ring, uint8_t *buf, uint16_t size);

uint16_t RING_GetCount(const RING_buffer_t *buf);

void RING_Clear(RING_buffer_t* buf);

void RING_Put( RING_buffer_t* buf, uint8_t symbol);

void RING_Put16( RING_buffer_t* buf, uint16_t symbol);

void RING_Put32( RING_buffer_t* buf, uint32_t symbol);

void RING_PutBuffr(RING_buffer_t *ringbuf, const uint8_t *src, uint16_t len);

uint8_t RING_Pop(RING_buffer_t *buf);

uint16_t RING_Pop16(RING_buffer_t *buf);

uint32_t RING_Pop32(RING_buffer_t *buf);

void RING_PopBuffr(RING_buffer_t *ringbuf, uint8_t *destination, uint16_t len);

void RING_PopString(RING_buffer_t *ringbuf, char *string);

int32_t RING_ShowSymbol(const RING_buffer_t *buf, uint16_t symbolNumber);

uint16_t crc16_X_Modern(const RING_buffer_t *buf, uint16_t lenght, uint16_t position);
#ifdef __cplusplus
}
#endif

#endif /* RING_BUFFER_H */
