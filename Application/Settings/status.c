/*
 * status.c
 *
 *  Created on: 6 апр. 2021 г.
 *      Author: Ilia
 */
#include "status.h"
static struct status dev_status;
struct status* status_get(){
	return &dev_status;
}
void status_init(void){
	struct settings* dev_settings = settings_get();
	dev_status.state = UHF;
	dev_status.tel_UHF_en = 1;
	dev_status.tel_sd_log = 1;
}
