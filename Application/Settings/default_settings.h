/*
 * defoult_settings.h
 *
 *  Created on: 29 ���. 2021 �.
 *      Author: Ilia
 */

#ifndef SETTINGS_DEFAULT_SETTINGS_H_
#define SETTINGS_DEFAULT_SETTINGS_H_
#define FLASH_APP_SIZE                      0x18000
#define FLASH_BOOTLOADER_ADDRESS            0x08000000
#define FLASH_APP_ADDRESS                   0x08008000
#define FLASH_CRC_APP                       (FLASH_APP_ADDRESS+FLASH_APP_SIZE-4)    //0x0801FFFC
#define FLASH_SECOND_APP_ADRESS             (FLASH_APP_ADDRESS+FLASH_APP_SIZE)      //0x08020000
#define FLASH_CRC_SECOND_APP                (FLASH_APP_ADDRESS+2*FLASH_APP_SIZE-4)  //0x08037FFC
#define FLASH_SETTINGS_ADDRESS              (FLASH_APP_ADDRESS+2*FLASH_APP_SIZE)    //0x08038000
#define FLASH_BOOTLOADER_FLAG               0x08038800
#define FLASH_LOGBOOK_ADDRESS               0x08039000      //233472 bytes from 256 kbyter for EPS and 1mbyte for OBC


#define UNICAN_GROUND_ADDRESS               0x1
#define UNICAN_EPS_ADDRESS                  0x4

#define DEV_SERIAL_LEN			            8
#define DEFAULT_DEVICE_SERIAL	            "Unknown"
#define DEFAULT_FW_VERSION		            19
#define DEFAULT_CAN_DEVICE_ID	            0x5
#define DEFAULT_CAN_ISL_FRIEND1_DEVICE_ID   12
#define DEFAULT_CAN_ISL_FRIEND2_DEVICE_ID   19
#define DEFAULT_CAN_ISL_FRIEND3_DEVICE_ID   26
#define DEFAULT_TEL_REG_PERIOD	            5000
#define DEFAULT_HEATBEAT_PEROOD             20000
#define DEFAULT_IWDG_PERIOD	                2000
#define DEFAULT_PLANNER_PERIOD	            2000

#define ISL_PACKAGE_NUM	                    5
#endif /* SETTINGS_DEFAULT_SETTINGS_H_ */
