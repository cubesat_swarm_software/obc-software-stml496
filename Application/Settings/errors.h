/*
 * errors.h
 *
 *  Created on: Sep 16, 2020
 *      Author: Данил
 */

#ifndef INC_ERRORS_H_
#define INC_ERRORS_H_
/* Private defines -----------------------------------------------------------*/
//errors ID
//CAN
#define ERR_CAN_FILTER_CONF		0x00
#define ERR_CAN_START			0x01
#define ERR_CAN_ACTIVATE_NOTIF	0x02

//I2C UI
#define ERR_INA219_BB_VCC 		0x10
#define ERR_INA219_COIL_VCC		0x11
#define ERR_INA219_OBC_3V3 		0x12
#define ERR_INA219_BATC1		0x13
#define ERR_INA219_CHRG1		0x14
#define ERR_INA219_BATC2		0x15
#define ERR_INA219_VBAT2		0x16
#define ERR_INA219_VBAT3		0x17
#define ERR_INA219_BATP			0x18
#define ERR_INA219_PCH1			0x19
#define ERR_INA219_PCH2			0x1A
#define ERR_INA219_PCH3			0x1B
#define ERR_INA219_PCH4			0x1C
#define ERR_INA219_CHRG2		0x1D
#define ERR_PAC1934_INIT		0x1E
#define ERR_PAC1934_GETTING		0x1F

#define ERR_TMP100_INIT			0x20
#define ERR_TMP100_GETTING		0x21
#define ERR_DS1631_INIT			0x22
#define ERR_DS1631_GETTING		0x23


//IMU
#define ERR_IMU_READING			0x30
#define ERR_IMU_WRITING			0x31
#define ERR_IMU_MAG				0x32
#define ERR_IMU_GYRO			0x33

#define ERR_SD_CARD_INIT     	0x38
#define ERR_SD_CARD_WRITE     	0x39
//RESET ERROR

#define ERR_IWDG_RESET          0x40
#define ERR_POWER_OFF           0x41

#define ERR_ANKNOWN_CMD         0x50
#define ERR_CMD_WRONG_PARAM     0x51

#define ERR_UNICAN_CRC              0x60
#define ERR_UNICAN_TOO_SHORT    0x61
#define ERR_UNICAN_TOO_LONG         0x62
#define ERR_UNICAN_TIMEOUT          0x63    //

#endif /* INC_ERRORS_H_ */

