/*
 * ISL.h
 *
 *  Created on: 29 мая 2021 г.
 *      Author: Ilia
 */

#ifndef COMMANDS_ISL_H_
#define COMMANDS_ISL_H_

#pragma push(pack)
#pragma pack(1)
struct ISL_{
	uint16_t session_num;     //4 byte
	uint16_t package;         // 2 byte
	struct time_board time; // 4 byte
	uint16_t param1;        // 2 byte
	uint16_t param2;        // 2 byte
	uint16_t param3;        // 2 byte
};
#pragma pop(pack)


#endif /* COMMANDS_ISL_H_ */
