/*
 * cmd_OBC.c
 *
 *  Created on: 9 апр. 2021 г.
 *      Author: Ilia
 */

#include "cmd_OBC.h"
#include "log_utils.h"
#include "settings.h"
#include "status.h"
#include "time.h"
#include "unican.h"
#include "cmd_msg_id.h"
static const char *tag = "cmd_OBC";
extern SemaphoreHandle_t xSemaphore_ISL;

int prob_cmd(uint8_t sender, uint8_t *data) {
	LOGI(tag, "I AM OBC");
    return 0;
}

int turn_off_telemetry_com(uint8_t sender, uint8_t *data){
	struct status* dev_status = status_get();
	LOGI(tag, "TEL COM OFF");
	dev_status->tel_UHF_en = 0;
	return 0;
}

int turn_on_telemetry_com(uint8_t sender, uint8_t *data){
	struct status* dev_status = status_get();
	dev_status->tel_UHF_en = 1;
	LOGI(tag, "TEL COM ON");
	return 0;
}

int turn_off_telemerty_logging(uint8_t sender, uint8_t *data){
	struct status* dev_status = status_get();
	dev_status->tel_sd_log = 0;
	return 0;
}

int turn_on_telemerty_logging(uint8_t sender, uint8_t *data){
	struct status* dev_status = status_get();
	dev_status->tel_sd_log = 1;
	return 0;
}

int start_ISL(uint8_t sender, uint8_t *data){
	if( xSemaphoreGive( xSemaphore_ISL ) == pdTRUE ){
		LOGI(tag, "OK ISL");
		return 0;
	}
	else{
		LOGI(tag, "FAIL ISL");
		return 1;
	}
}

int give_status(uint8_t sender, uint8_t *data) {
	struct status *dev_status = status_get();
	struct unican_message msg;
	uint8_t sd_status = dev_status->sens_err[SENS_OBC_SD_CARD];
	msg.data = (uint8_t*)(&sd_status);
	msg.unican_address_to = sender;
	msg.unican_length = 1;
	msg.unican_msg_id = RESP_GET_STATUS;
	unican_send_msg(&msg);
	return 0;
}

int set_onboard_time(uint8_t sender, uint8_t *data){
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;
	struct time_board *time;
	time = data;
	sDate.Date  = time->days;
	sTime.Hours = time->hours;
	sTime.Minutes = time->minutes;
	sTime.Seconds = time->seconds;
	time_set(&sTime, &sDate);
	LOGI(tag,"SET TIME");
	return 0;
}
