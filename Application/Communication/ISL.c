/*
 * ISL.c
 *
 *  Created on: 3 июн. 2021 г.
 *      Author: Ilia
 */

#include "ISL.h"
#include "unican.h"
#include "log_sd.h"
#include "cmd_msg_id.h"
#include "log_utils.h"
#include "default_settings.h"
extern struct unican_handler ISL_dispatcher;
static const char * tag = "ISL";

void ISL_start(uint8_t address){
	struct unican_message msg;
	msg.unican_length = 0;
	msg.unican_msg_id = 11111;
	msg.unican_address_to = address;
	unican_send_msg(&msg);
}
void ISL_test(uint8_t address, uint16_t isl_session_number, uint8_t isl_counter){
	uint8_t isl_feedback = 0;
	char sd_log_str[50] = "";
	if (unican_receive(&ISL_dispatcher, 2000)) {
		osDelay(1000);
		if((ISL_dispatcher.packet.msg_id == CMD_ISL_PACKAGE)&&(ISL_dispatcher.packet.sender == address)){
			LOGI(tag, "GOT ISL FEEDBACK");
			isl_feedback = ISL_OK;
		}
		else{
			isl_feedback = ISL_WRONG;
			LOGI(tag, "GOT WRONG FEEDBACK");
		}
	}
	else{
		LOGI(tag, "DON'T GET ISL FEEDBACK");
		isl_feedback = ISL_TIMEOUT;
	}

	sprintf(sd_log_str,"Session %d, address %d, Package %d, res %d\n",isl_session_number,address, isl_counter+1,isl_feedback);
	log_sd_str("ISL.txt", &sd_log_str, strlen(sd_log_str));
	struct unican_message msg;

	msg.data = (uint8_t *)&isl_session_number;
	//msg.data[1] = 0;//(uint8_t)((isl_session_number&&0xFF00)>>8);
	msg.unican_length = 2;
	msg.unican_msg_id = 11111;
	msg.unican_address_to = address;
	bool res = unican_send_msg(&msg);
	if(res==0){
		LOGI(tag, "FAIL SEND ISL");
	}
}
