/*
 * ISL.h
 *
 *  Created on: 3 июн. 2021 г.
 *      Author: Ilia
 */

#ifndef COMMUNICATION_ISL_H_
#define COMMUNICATION_ISL_H_
#include "stm32l4xx_hal.h"
void ISL_start(uint8_t address);
void ISL_test(uint8_t address, uint16_t isl_session_number, uint8_t isl_counter);
typedef enum {ISL_OK = 0, ISL_TIMEOUT = 1, ISL_WRONG = 2} isl_rezult;
#endif /* COMMUNICATION_ISL_H_ */
