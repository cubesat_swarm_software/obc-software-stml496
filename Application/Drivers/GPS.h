/*
 * GPS.h
 *
 *  Created on: 29 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef INC_GPS_H_
#define INC_GPS_H_

#include "main.h"
#include "time.h"
#include "stdbool.h"

#define N_GPS_DATA             100
#define N_GPS_DATA_DMA         512
#define N_GPS_STREAM_BUF_SIZE  256

#define GPS_SYNC_WORD_L         0x81
#define GPS_SYNC_WORD_R         0xFF
#define GPS_FRAME_ID_NUMBER   1
#define GPS_DATA_SIZE_NUMBER  2
#define GPS_CRC_HEAD_NUMBER   4

#define GPS_FRAME_HEADER_BYTES 5*2

#define GPS_FRAME_ID_UTC      3000  //UTC and position
#define GPS_FRAME_ID_16CH     3001
#define GPS_FRAME_ID_24CH     3011
#define GPS_FRAME_ID_ALM      3002
#define GPS_FRAME_ID_DIF      3003
#define GPS_FRAME_ID_CMD      3006
#define GPS_FRAME_ID_CNTRL    2000
#define GPS_FRAME_ID_KVT      2200

#define GPS_LATITUDE_POS      0
#define GPS_LATITUDE_N        4
#define GPS_LONGITUDE_POS     4
#define GPS_LONGITUDE_N       4
#define GPS_ALTITUDE_POS      8
#define GPS_ALTITUDE_N        4
#define GPS_YAER_POS          28
#define GPS_MONTH_POS         30
#define GPS_DAY_POS           32
#define GPS_HOUR_POS          34
#define GPS_MINUTE_POS        36
#define GPS_SECOND_POS        38
#define GPS_FLAGS_POS         72
#pragma push(pack)
#pragma pack(1)
struct gps_data {
	double latitude;
	double longitude;
	double altitude;
	struct time_board time;
};
#pragma pop(pack)

void GPS_module_init(UART_HandleTypeDef * huart);
bool GPS_get_data(struct gps_data *data, uint32_t timeout);

#endif /* INC_GPS_H_ */
