#include "Sun_sensor.h"
#include "log_utils.h"
#include "stream_buffer.h"


#define PACKAGE_SIZE     8
#define DATA_DMA_SIZE    8
#define STREAM_BUF_SIZE  16
#define START_BYTE       0x3A
static struct {
	UART_HandleTypeDef * uart;
	uint8_t				 dma_storage[DATA_DMA_SIZE];
    uint8_t              BufferStorage[STREAM_BUF_SIZE];
    StaticStreamBuffer_t StreamBuffer_struct;
    StreamBufferHandle_t StreamBuffer;
} Sun_state;

static void uart_rx_cb(void);


void Sun_sensor_init(UART_HandleTypeDef * huart){
	Sun_state.uart = huart;
	HAL_UART_RegisterCallback(Sun_state.uart, HAL_UART_RX_COMPLETE_CB_ID, uart_rx_cb);
	Sun_state.StreamBuffer = xStreamBufferCreateStatic( sizeof( Sun_state.BufferStorage ),
	                                               1,
												   Sun_state.BufferStorage,
	                                               &Sun_state.StreamBuffer_struct );

}

bool Sun_sensor_get_data(struct sun_sensor_data *data, uint32_t timeout){
	HAL_UART_Receive_DMA(Sun_state.uart, &Sun_state.dma_storage, PACKAGE_SIZE);
	size_t xReceivedBytes = 0;
	uint8_t raw_data[PACKAGE_SIZE];
	xReceivedBytes = xStreamBufferReceive(Sun_state.StreamBuffer,
											( void * ) raw_data,
											 PACKAGE_SIZE,
											 pdMS_TO_TICKS(timeout) );
	data->angle1 = ((uint16_t)(raw_data[1] << 8) | raw_data[0]);
	return (xReceivedBytes>0);
}


void uart_rx_cb(void){
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	if(Sun_state.dma_storage[0] == START_BYTE){
		uint8_t counter = Sun_state.dma_storage[1];
		size_t xBytesSent = xStreamBufferSendFromISR( Sun_state.StreamBuffer,
													 ( void *)&Sun_state.dma_storage[2],
													 PACKAGE_SIZE,
													 &xHigherPriorityTaskWoken);
	}

}
