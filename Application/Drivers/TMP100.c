/*
 * TMP100.c
 *
 *  Created on: Sep 30, 2020
 *      Author: Р”Р°РЅРёР»
 */


#include "TMP100.h"

#define TMP100_CONFIG   {data = 0;}
#define TMP117_CONFIG   {data = 0;}
#define TMP1075_CONFIG  {data = 0;}
#define I2C_TIMEOUT  1000
uint8_t TMP100_Init(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t sens_model)
{
	uint8_t data = 0;
	uint8_t reg = 0;
	uint8_t n_bytes = 1;
	switch (sens_model)
	{
	  case TMP100: reg = TMP100_CONF_REG; n_bytes = 1; TMP100_CONFIG; break;
	  case TMP117: reg = TMP117_CONF_REG; n_bytes = 2; TMP117_CONFIG; break;
	  case TMP1075: reg = TMP1075_CONF_REG; n_bytes = 2; TMP1075_CONFIG; break;

	  default: break;
	}

	//uint8_t data = 0;
	if (HAL_I2C_Mem_Write(hi2c, address << 1, reg, 1, &data, 1, 1000)==HAL_OK){
		return 0;
	}
	else{
		return 1;
	}
}

uint8_t TMP100_GetTemperature(I2C_HandleTypeDef *hi2c, uint8_t address, int16_t *value, uint8_t sens_model)
{
	uint8_t i2c_state;
        int16_t data;
        uint8_t reg = 0;

        switch (sens_model)
        {
          case TMP100: reg = TMP100_TEMP_REG; break;
          case TMP117: reg = TMP117_TEMP_REG; break;
          case TMP1075: reg = TMP1075_TEMP_REG; break;

          default: break;
        }

	uint8_t buf[2] = {reg, 0};
	while((i2c_state = HAL_I2C_Master_Transmit(hi2c, address << 1, buf, 1, I2C_TIMEOUT)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
	{
                *value = (int16_t) 0xffff;
                return 1;
        }
	//HAL_Delay(5);
	while((i2c_state = HAL_I2C_Master_Receive(hi2c, address << 1, buf, 2, I2C_TIMEOUT)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
	{
                *value = (int16_t) 0xffff;
                return 1;
        }

	data = (buf[0] << 8) + buf[1];

	switch (sens_model)
        {
          case TMP100: *value = (int16_t) (((data*10) >> 4) >> 4); break;
          case TMP117: *value = (int16_t) (((data*10) >> 4) >> 3); break;
          case TMP1075: *value = (int16_t) (((data*10) >> 4) >> 4); break;

          default: break;
        }

        return 0;
}
