/*
 * TMP100.h
 *
 *  Created on: Sep 30, 2020
 *      Author: Р”Р°РЅРёР»
 */

#include "stm32l4xx_hal.h"
#include "stdlib.h"
#include "main.h"

#ifndef INC_TMP100_H_
#define INC_TMP100_H_

#define TMP100_COILXY           0b1001000
#define TMP100_COILZ            0b1001010
#define TMP100_IMU              0b1001100
#define TMP100_SW_3V3           0b1001110


#define TMP100_TEMP_REG		0x00
#define TMP100_CONF_REG		0x01
#define TMP100_TLOW_REG		0x02
#define TMP100_THIGH_REG	0x03

//Configuration Register
#define TMP100_CONF_SD		0
#define TMP100_CONF_TM		1
#define TMP100_CONF_POL		2
#define TMP100_CONF_F0		3
#define TMP100_CONF_F1		4
#define TMP100_CONF_R0		5
#define TMP100_CONF_R1		6
#define TMP100_CONF_OS		7

#define TMP1075_CONF_REG        0x01
#define TMP1075_TEMP_REG        0x00

#define TMP1075_XP_IN           0b1011000
#define TMP1075_XN_IN           0b1011001
#define TMP1075_YP_IN           0b1011010
#define TMP1075_YN_IN           0b1011011
#define TMP1075_XP_FEP          0b1011100
#define TMP1075_XN_FEP          0b1011101
#define TMP1075_YP_FEP          0b1011110
#define TMP1075_YN_FEP          0b1011111

#define TMP117_CONF_REG         0x01
#define TMP117_TEMP_REG         0x00

#define TMP117_XP_OUT           0b1001000
#define TMP117_XN_OUT           0b1001001
#define TMP117_YP_OUT           0b1001010
#define TMP117_YN_OUT           0b1001011


#define TMP100  0
#define TMP117  1
#define TMP1075 2

#endif /* INC_TMP100_H_ */

uint8_t TMP100_Init(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t sens_model);
uint8_t TMP100_GetTemperature(I2C_HandleTypeDef *hi2c, uint8_t address, int16_t *value, uint8_t sens_model);
