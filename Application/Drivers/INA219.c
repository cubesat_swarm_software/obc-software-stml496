#include "INA219.h"

uint32_t _calValue;
// The following multipliers are used to convert raw current and power
// values to mA and mW, taking into account the current config settings
uint32_t _currentDivider_mA;
float    _powerMultiplier_mW;
float	 _voltageDivider_mV;



uint8_t wireWriteRegister (I2C_HandleTypeDef *hi2c, uint8_t address,uint8_t reg, uint16_t value)
{
	uint8_t buffer[2] = {(uint8_t)((value >> 8) & 0xFF), // Upper 8-bits
						 (uint8_t)(value & 0xFF) };     // Lower 8-bits
	if (HAL_I2C_Mem_Write(hi2c, address<< 1,reg,1,buffer,2,1000)==HAL_OK){
		return 0;
	}
	else{
		return 1;
	}
}

uint8_t wireReadRegister(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg, uint16_t *value)
{
	uint8_t buffer[2];
    // Shift values to create properly formed integer
	if (HAL_I2C_Mem_Read(hi2c, address<< 1,reg,1,buffer,2,1000)==HAL_OK){
		*value = (((uint16_t)buffer[0] << 8) | buffer[1]); // can maybe be replaced by just a cast into uint16_t ??
		 //uint8_t strbyte[15];
		 //sprintf(strbyte, "  two bytes %d : %d\r\n",buffer[0],buffer[1] );
		 //HAL_UART_Transmit(&huart1, strbyte, strlen((char *)strbyte), 100);
		return 0;
		}
	else{
		return 1;
	}
}

uint8_t INA219_SetCalibration(I2C_HandleTypeDef *hi2c, uint8_t address)
{
	uint8_t success_calibrate;
	uint8_t success_config;

	  // Calibration which uses the highest precision for
	  // current measurement (0.1mA), at the expense of
	  // only supporting 16V at 400mA max.

	  // VBUS_MAX = 16V
	  // VSHUNT_MAX = 0.04          (Assumes Gain 1, 40mV)
	  // RSHUNT = 0.05              (Resistor value in ohms)

	  // 1. Determine max possible current
	  // MaxPossible_I = VSHUNT_MAX / RSHUNT
	  // MaxPossible_I = 1.6A (2A)

	  // 2. Determine max expected current
	  // MaxExpected_I = 1.6A  (2A)

	  // 3. Calculate possible range of LSBs (Min = 15-bit, Max = 12-bit)
	  // MinimumLSB = MaxExpected_I/32767
	  // MinimumLSB = 0.0000488              (24uA per bit) 0.
	  // MaximumLSB = MaxExpected_I/4096
	  // MaximumLSB = 0.0004              (195uA per bit)

	  // 4. Choose an LSB between the min and max values
	  //    (Preferrably a roundish number close to MinLSB)
	  // CurrentLSB = 0.0002 (100uA per bit) (100) 0

	  // 5. Compute the calibration register
	  // Cal = trunc (0.04096 / (Current_LSB * RSHUNT))
	  // Cal = 4096 (0x2000)

	  _calValue = 4096;//8192;

	  // 6. Calculate the power LSB
	  // PowerLSB = 20 * CurrentLSB
	  // PowerLSB = 0.001 (1mW per bit)

	  // 7. Compute the maximum current and shunt voltage values before overflow
	  //
	  // Max_Current = Current_LSB * 32767
	  // Max_Current = 1.63835A before overflow
	  //
	  // If Max_Current > Max_Possible_I then
	  //    Max_Current_Before_Overflow = MaxPossible_I
	  // Else
	  //    Max_Current_Before_Overflow = Max_Current
	  // End If
	  //
	  // Max_Current_Before_Overflow = MaxPossible_I
	  // Max_Current_Before_Overflow = 0.4
	  //
	  // Max_ShuntVoltage = Max_Current_Before_Overflow * RSHUNT
	  // Max_ShuntVoltage = 0.04V
	  //
	  // If Max_ShuntVoltage >= VSHUNT_MAX
	  //    Max_ShuntVoltage_Before_Overflow = VSHUNT_MAX
	  // Else
	  //    Max_ShuntVoltage_Before_Overflow = Max_ShuntVoltage
	  // End If
	  //
	  // Max_ShuntVoltage_Before_Overflow = VSHUNT_MAX
	  // Max_ShuntVoltage_Before_Overflow = 0.04V

	  // 8. Compute the Maximum Power
	  // MaximumPower = Max_Current_Before_Overflow * VBUS_MAX
	  // MaximumPower = 0.4 * 16V
	  // MaximumPower = 6.4W

	  // Set multipliers to convert raw current/power values
	_powerMultiplier_mW = 1.0f;	// Power LSB = 1mW per bit
	_currentDivider_mA = 5.0;	// Current LSB = 200uA per bit (1000uA/200uA = 5)
								//To obtain a value in amperes the Current register value is multiplied by the programmed Current_LSB.
	_powerMultiplier_mW = 1.0f;	// Power LSB = 1mW per bit (2/1)
	_voltageDivider_mV = 4;//mV

	// Set Calibration register to 'Cal' calculated above
	success_calibrate = wireWriteRegister(hi2c, address, REG_CALIBRATION, _calValue);

	// Set Config register to take into account the settings above
	  uint16_t config = CONFIG_BVOLTAGERANGE_32V |
			  	  	  	CONFIG_GAIN_8_320MV |
	                    CONFIG_BADCRES_12BIT |
						CONFIG_SADCRES_12BIT_128S_69MS |
						CONFIG_MODE_SANDBVOLT_CONTINUOUS;
	success_config = wireWriteRegister(hi2c, address, REG_CONFIG, config);
	if ((success_calibrate==0)&&(success_config==0)){
		return 0;
	}
	else{
		return 1;
	}
}

uint8_t getCurrent_raw(I2C_HandleTypeDef *hi2c, uint8_t address,int16_t * rawValue) {
	uint16_t value = 0;
	uint8_t success_calibrate;
	uint8_t success_read;
	success_calibrate =  wireWriteRegister(hi2c, address,REG_CALIBRATION, _calValue);
	success_read= wireReadRegister(hi2c, address,REG_CURRENT, &value);
	if ((success_calibrate==0)&&(success_read==0)){
	  *rawValue = (int16_t)value;
	  return 0;
	}
	else
	{
	  return 1;
	}
}
float getShuntVoltage_raw(I2C_HandleTypeDef *hi2c, uint8_t address) {
  float v;
  uint16_t value = 0;
  wireReadRegister(hi2c, address,REG_SHUNTVOLTAGE, &value);
  v = value*0.01;
  return v;
}


uint8_t getBusVoltage_raw(I2C_HandleTypeDef *hi2c, uint8_t address,int16_t * rawValue) {
	uint16_t value = 0;
	uint8_t success_calibrate;
	uint8_t success_read;
	success_calibrate =  wireWriteRegister(hi2c, address,REG_CALIBRATION, _calValue);
	success_read= wireReadRegister(hi2c, address,REG_BUSVOLTAGE, &value);
	if ((success_calibrate==0)&&(success_read==0)){
	  *rawValue = (int16_t)value;
	  return 0;
	}
	else
	{
	  return 1;
	}
}


uint8_t getCurrentAmps(I2C_HandleTypeDef *hi2c, uint8_t address,float * amps) {
	int16_t valueRaw;
	float valueDec;
	uint8_t success = getCurrent_raw(hi2c, address,&valueRaw);
	valueDec = valueRaw;
	valueDec /= _currentDivider_mA;
	if (success==0){
		*amps = valueDec;
		return 0;
	}
	else
	{
	  return 1;
	}
}
/*
uint8_t getVoltage(uint8_t address,float * volts) {
	int16_t valueRaw;
	float valueDec;
	uint8_t success = getBusVoltage_raw(address,&valueRaw);
	valueDec = valueRaw;
	valueDec = valueDec*_voltageDivider_mV/1000;
	if (success==0){
		*volts = valueDec;
		return 0;
	}
	else
	{
	  return 1;
	}
}
*/

uint8_t INA219_GetUI(I2C_HandleTypeDef *hi2c, uint8_t address, uint16_t *volts, uint16_t *amps) {
	uint16_t valueCur, valueVol;
	uint8_t success_calibrate;
	uint8_t success_read = 0;

	success_calibrate =  wireWriteRegister(hi2c, address,REG_CALIBRATION, _calValue);
	success_read |= wireReadRegister(hi2c, address,REG_BUSVOLTAGE, &valueVol);
	success_read &= wireReadRegister(hi2c, address,REG_CURRENT, &valueCur);


	if ((success_calibrate==0)&&(success_read==0))
	{
		if (valueCur >= 32000)
			*amps = ((uint16_t)~(valueCur-1)) / _currentDivider_mA;
		else
			*amps = valueCur / _currentDivider_mA;

		*volts = (valueVol >> 3) * _voltageDivider_mV;
		return 0;
	}
	else
	{
		return 1;
	}
}
